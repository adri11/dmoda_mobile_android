/* proabilmente eliminabile
window.Message=Backbone.Model.extend ({
    defaults: {
        "content":"",
        "datetime":null
    }
});
window.MessageCollection=Backbone.Model.extend ({
    urlRoot:"http://127.0.0.1/rest/messageList", //TODO cambiare con l'url giusto
    model:Message
});
*/

window.MessageModel = {
    url:urlServer+"/rest",

    getList:function (successCallback, errorCallback,restToken) {

        $.ajax({
            type: "GET",
            contentType: "application/x-www-form-urlencoded",
            //in ms
            timeout:10000,
            url:this.url,
            data:{
                "id":restToken

            },
            success:function(result){

                if ( successCallback ) {
                    successCallback( result );
                }
            },
            error:function(error){
                console.error("errore ajax"+error);
                if ( errorCallback ){
                    errorCallback( error );
                }
            }
        });

    }
}
