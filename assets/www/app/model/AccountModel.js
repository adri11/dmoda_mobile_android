
window.AccountModel = {

    url:urlServer+"/rest/account",

    login:function (successCallback, errorCallback,dati) {

        $.ajax({
            type: "POST",
            //in ms
            timeout:10000,
            url:this.url,
            data:dati,
            success:function(result){
                if ( successCallback ) {
                    successCallback( result );
                }
            },
            error:function(error){
                console.error("errore ajax"+error);
                if ( errorCallback ){
                    errorCallback( error );
                }
            }
        });
    }
}
