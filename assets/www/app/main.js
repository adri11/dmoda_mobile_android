document.addEventListener("deviceready", onDeviceReady, false);


function onDeviceReady() {

    //console.log( "READY" );

    window.GeoWatcher.watch();
    //carica il CONTENUTO (sostituendo il path) di tutti i file di template nell'oggetto templates, come templates[nomeTemplate] es. templates[homeView]
    // e al termine esegue appTemplatesLoaded
    loadTemplates( appTemplatesLoaded );
}
//TODO togliere quando si deploya
onDeviceReady();
//

function appTemplatesLoaded() {
    //console.log( "VIEW TEMPLATES LOADED" );

    $("body").empty();

    //Setup the ViewNavigator
    window.viewNavigator = new ViewNavigator( 'body' );

    var homeView = new HomeView();
    var loginView = new LoginView();

    if (localStorage.getItem('rest-token')) {
        window.viewNavigator.pushView( homeView );
    }
    else {
        window.viewNavigator.pushView( loginView );
    }

    document.addEventListener("backbutton", onBackKey, false);
}

function onBackKey( event ) {
    if ( window.viewNavigator.history.length > 1 ){
        event.preventDefault();
        window.viewNavigator.popView();
        return false;
    }
    navigator.app.exitApp();
}

document.addEventListener('touchmove', function (e) { e.preventDefault(); }, false);
