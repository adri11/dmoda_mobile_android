templates.messageListView = "app/views/MessageListView.html";

window.MessageListView = Backbone.View.extend({
    //used by viewnavigator for the header of ui
    title: "Messaggi in arrivo",
    destructionPolicy:'never',

    initialize: function(options) {
        this.render();
        //this.$el=A cached jQuery object for the view's element. A handy reference instead of re-wrapping the DOM element all the time.
        this.model=options;
        this.view = this.$el;
    },  
    
    events:{
    },
    //TODO Formattare meglio la data e l'ora
    //TODO Fare paginazione? Gestire meglio la quantità di dati da ricevere?
    render:function (eventName) {
        // http://underscorejs.org/#template
        // stringa contenente il contenuto del file di template
        var template = _.template(templates.messageListView);
        this.$el.css("background", "white");
        this.$el.html(template( {elementsLength:this.model.length}));
        var $list = this.$el.find("#list");
        var self = this;
        var index = 1;
        _.each(this.model, function (message) {
           $list.append(new MessageListItemView({model:message}).render().el);
            index += 1;
        }, this);
        return this;
    },

    home:function(){
        var home=new HomeView();
        window.viewNavigator.pushView(home);
    }
});