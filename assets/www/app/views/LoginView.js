templates.loginView = "app/views/LoginView.html";

window.LoginView = Backbone.View.extend({
    //used by viewnavigator for the header of ui
    //E' la parte grafica in alto
    title: "D Moda Mobile",
    destructionPolicy:'never',

    initialize: function(options) {
        this.render();
        this.view = this.$el;
    },  
    
    events:{
        //"submit form":"submit",
        "submit form":"submit"
    },
    
    render:function (eventName) {
        var template = _.template(templates.loginView);
        var model = {isTablet:NativeUtil.isTablet()};
        //this.$el.html(template(model));
        if ((typeof(this.options)!="undefined")&&(typeof(this.options.error)!="undefined")) {
            this.error=this.options.error;
        }
        this.$el.html(template({error:this.error}));

        if ( model.isTablet ) {
            this.$el.css("height", "100%");
            this.$el.css("margin", "0px");
            this.$el.css("padding", "0px");
            this.$el.find("#loginView").css("height", "100%");


            var wrapper = this.$el.find("#wrapper");
            wrapper.css("margin", "0px");
            wrapper.css("padding", "0px");
            
            var well = this.$el.find(".well");
            well.css("width", "460px");
            well.css("margin", "0px");
            well.css("padding", "30px");
            well.find(".input-append").css("max-width", "101%");

            well.css("position", "absolute");
            var hOffset = ($(window).width() - well.width())-70;
            
            hOffset = hOffset/2;
            /*
            if ( $(window).width() > 700) {
            	hOffset = hOffset/3;
            }
            else {
            	hOffset = hOffset/4;
            }*/
            
            well.css("left", hOffset+"px");
            well.css("top", "180px");
        }



        var self = this;

        return this;
    },
    submit:function(e){
        e.preventDefault();
        var username=$('#username').val();
        var password=$('#password').val();
        var dati={
            "user":username,
            "pwd":password
        };
        var loginProgress=new LoginProgressView(dati);

        window.viewNavigator.pushView(loginProgress);
    }
});