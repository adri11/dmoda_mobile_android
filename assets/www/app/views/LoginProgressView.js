templates.loginProgressView = "app/views/LoginProgressView.html";

window.LoginProgressView = Backbone.View.extend({

    title: "Searching...",

    initialize: function (options) {
        var dati=options;
        this.render();
        this.view = this.$el;
        var self = this;


        //delay long enough for transition to complete
        setTimeout(function () {
            AccountModel.login(self.rispostaRicevuta, self.rispostaFallita,dati);
        }, 401);
    },

    events: {
    },

    render: function (eventName) {
        this.$el.html(templates.searchView);

        this.$el.css("height", "100%");
        return this;
    },

    rispostaRicevuta: function (result) {
        var jsonResult = JSON.parse(result);
        if (jsonResult['status']==='error') {
            setTimeout(function () {
                var view = new LoginView ({error: true});
                window.viewNavigator.replaceView(view);
            }, 550);
        }
        else if (jsonResult['rest-token']){
            localStorage.setItem('rest-token',jsonResult['rest-token']);

            setTimeout(function () {
                var view = new HomeView ();
                window.viewNavigator.replaceView(view);
            }, 550);


        }
        else {
            setTimeout(function () {
                var view = new LoginView ({error: true});
                window.viewNavigator.replaceView(view);
            }, 550);
        }
    },

    rispostaFallita: function (error) {
        setTimeout(function () {
            var view = new LoginView ({error: true});
            window.viewNavigator.replaceView(view);
        }, 550);
    }



});