templates.messageListItemView = "app/views/MessageListItemView.html";
//TODO Eliminazione messaggi?
window.MessageListItemView = Backbone.View.extend({

    //tagName:'li',
    template:undefined,

    initialize: function(options) {

        this.template = _.template( templates.messageListItemView ),
            this.render();
        this.view = this.$el;
    },

    events:{
    },

    render:function (eventName) {
        var model = this.model;
        this.$el.html( this.template( model ));

        return this;
    }
});