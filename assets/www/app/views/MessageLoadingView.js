templates.messageLoadingView = "app/views/MessageLoadingView.html";

window.MessageLoadingView = Backbone.View.extend({

    title: "Searching...",

    initialize: function (options) {

        this.render();
        this.view = this.$el;
        var self = this;


        //delay long enough for transition to complete
        setTimeout(function () {
            MessageModel.getList(self.rispostaRicevuta, self.rispostaFallita,localStorage.getItem('rest-token'));
        }, 401);
    },

    events: {
    },

    render: function (eventName) {
        this.$el.html(templates.searchView);

        this.$el.css("height", "100%");
        return this;
    },

    rispostaRicevuta: function (result) {
        var jsonResult = JSON.parse(result);
        console.log(jsonResult);
        var view = new MessageListView({ model: jsonResult});
        window.viewNavigator.replaceView(view);

    },

    rispostaFallita: function (error) {
        var self = this;

        //wait for transition to finish, then cleanup once removed from view
        setTimeout(function () {

            var view = new HomeView ({error: error});
            window.viewNavigator.replaceView(view);
        }, 550);
    }



});